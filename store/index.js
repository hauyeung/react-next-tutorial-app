import { contactsReducer } from "./reducers";
import { createStore, combineReducers } from "redux";

const addressBookApp = combineReducers({
  contacts: contactsReducer,
});

const makeStore = (initialState, options) => {
  return createStore(addressBookApp, initialState);
};

export { makeStore };
